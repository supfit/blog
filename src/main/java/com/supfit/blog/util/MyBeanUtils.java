package com.supfit.blog.util;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import java.beans.PropertyDescriptor;
import java.util.ArrayList;
import java.util.List;

/**
 * @author supfit
 */
public class MyBeanUtils {

    /**
     * 获取所有属性值为空的属性名数组
     * @param source
     * @return
     */
    public static String[] getNullPropertyNames(Object source){
        BeanWrapper beanWrapper = new BeanWrapperImpl(source);
        PropertyDescriptor[] propertyDescriptors = beanWrapper.getPropertyDescriptors();
        List<String > nullPropertyNames = new ArrayList<>();
        for (PropertyDescriptor pd:
             propertyDescriptors) {
            String properName = pd.getName();
            if (beanWrapper.getPropertyValue(properName) == null){
                nullPropertyNames.add(properName);
            }
        }
        return nullPropertyNames.toArray(new String[nullPropertyNames.size()]);
    }
}
