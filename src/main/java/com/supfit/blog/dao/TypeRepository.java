package com.supfit.blog.dao;

import com.supfit.blog.po.Type;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author supfit
 */
public interface TypeRepository extends JpaRepository<Type,Long> {


    List<Type> findByName(String name);

    @Query("select t from Type t")
    List<Type> findTop(Pageable pageable);
}
