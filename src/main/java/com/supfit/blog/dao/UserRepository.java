package com.supfit.blog.dao;

import com.supfit.blog.po.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author supfit
 */
public interface UserRepository extends JpaRepository<User,Long> {

    User findByUsernameAndPassword(String username,String password);
}
