package com.supfit.blog.controller;

import com.supfit.blog.service.BlogService;
import com.supfit.blog.service.TagService;
import com.supfit.blog.service.TypeService;
import javassist.runtime.Desc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;

/**
 * @author supfit
 *
 */

@Controller
public class IndexController {

    @Autowired
    private BlogService blogService;
    @Autowired
    private TypeService typeService;
    @Autowired
    private TagService tagService;

//    @GetMapping("/{id}/{name}")
//    public String index(@PathVariable Integer id,@PathVariable String name){
//        int i = 9/0;
//        String blog = null;
//
//        if (blog == null){
//            throw new NotFoundException("博客不存在！");
//        }

    @GetMapping("/")
    public String index(@PageableDefault(size = 7,sort = {"updateTime"},direction = Sort.Direction.DESC) Pageable pageable, HttpSession session, Model model){
        System.out.println("-----------index----------");

        model.addAttribute("page",blogService.listBlog(pageable));
        model.addAttribute("types",typeService.listTypeTop(7));
        model.addAttribute("tags",tagService.listTagTop(11));
        model.addAttribute("recommendBlogs",blogService.listBlogTop(8));

        if (session.getAttribute("user") == null) {
            session.setAttribute("loginInfo","请登录！");
            return "index";
        }else {
            session.removeAttribute("loginInfo");
            return "index";
        }
    }

    @GetMapping("/blog/{id}")
    public String blog(@PathVariable Long id, Model model, RedirectAttributes redirectAttributes){
        if (blogService.idIsExist(id)){
            model.addAttribute("blog",blogService.getAndConvert(id));
            return "blog";
        }
        redirectAttributes.addFlashAttribute("message","该博客已被删除！");
        return "redirect:/";

    }

    @GetMapping("/admin/index")
    public String toAdmin(){

        return "admin/index";
    }


    @GetMapping("/login")
    public String toLogin(){

        return "redirect:/admin";
    }

    @GetMapping("/footer/newBlog")
    public String newBlogs(Model model){

        model.addAttribute("newBlogs",blogService.listBlogTop(4));
        return "_fragments :: newBlogList";
    }

    @PostMapping("/search")
    public String search(@PageableDefault(size = 7,sort = {"updateTime"},direction = Sort.Direction.DESC) Pageable pageable, @RequestParam String query, Model model){

        model.addAttribute("page",blogService.listBlog("%"+query+"%",pageable));
        model.addAttribute("query",query);
        return "search";
    }
}
