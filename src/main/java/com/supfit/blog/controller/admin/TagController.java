package com.supfit.blog.controller.admin;

import com.supfit.blog.po.Tag;
import com.supfit.blog.po.Type;
import com.supfit.blog.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;


/**
 * @author supfit
 */

@Controller
@RequestMapping("/admin")

public class TagController {

    @Autowired
    private TagService tagService;


    @GetMapping("/tags")
    public String types(@PageableDefault(size = 10,sort = {"id"},direction = Sort.Direction.DESC) Pageable pageable, Model model){

        model.addAttribute("page",tagService.listTag(pageable));
        System.out.println(tagService.listTag(pageable));
        return "admin/tags";
    }

    @GetMapping("/tags/input")
    public String input(Model model){
        model.addAttribute("tag",new Type());
        return "admin/tags-input";
    }

    @GetMapping("/tags/{id}/input")
    public String editInput(@PathVariable Long id, Model model){
        model.addAttribute("tag",tagService.getTag(id));
        return "admin/tags-input";
    }

    @PostMapping("/tags")
    /**
     * @Valid 对tag进行校验
     * BindingResult result 校验结果
     * 这两个一定要紧挨着
     */
    public String post(@Valid Tag tag, BindingResult result, RedirectAttributes redirectAttributes){
        if(result.hasErrors()){
            return "admin/tags-input";
        }
        List<Tag> tag1 = tagService.getTagByName(tag.getName());
        if (tag1 !=null && !tag1.isEmpty()){
            result.rejectValue("name","nameExist","该标签已存在！！");
            return "admin/tags-input";
        }
        Tag tag2 = tagService.saveTag(tag);
        if (tag2 == null ){
//            redirectAttributes.addFlashAttribute("message","分类新增失败！！！");
            result.rejectValue("name","addError","标签新增失败！！！");
            return "admin/tags-input";
        }else{
            redirectAttributes.addFlashAttribute("message","新增标签成功！");
            return "redirect:/admin/tags";
        }

    }

    @PostMapping("/tags/{id}")
    public String editPost(@Valid Tag tag, BindingResult result,@PathVariable Long id, RedirectAttributes redirectAttributes){
        if(result.hasErrors()){
            return "admin/tags-input";
        }
        List<Tag> tag1 = tagService.getTagByName(tag.getName());
        if (tag1 !=null && !tag1.isEmpty()){
            result.rejectValue("name","nameExist","该标签已存在！！");
            return "admin/tags-input";
        }
        Tag tag2 = tagService.updateTag(id,tag);
        if (tag2 == null ){
//          redirectAttributes.addFlashAttribute("message","分类新增失败！！！");
            result.rejectValue("name","addError","标签修改失败！！！");
            return "admin/tags-input";
        }else{
            redirectAttributes.addFlashAttribute("message","标签修改成功！");
            return "redirect:/admin/tags";
        }

    }

    @GetMapping("/tags/{id}/delete")
    public String delete(@PathVariable Long id,RedirectAttributes attributes){

        if (!tagService.idIsExist(id)){
            attributes.addFlashAttribute("message","标签已不存在！！");
            return "redirect:/admin/tags";
        }
        tagService.deleteTag(id);
        attributes.addFlashAttribute("message","标签删除成功！");
        return "redirect:/admin/tags";
    }
}
