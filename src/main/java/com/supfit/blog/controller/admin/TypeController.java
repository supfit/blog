package com.supfit.blog.controller.admin;

import com.supfit.blog.po.Type;
import com.supfit.blog.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;


/**
 * @author supfit
 */

@Controller
@RequestMapping("/admin")

public class TypeController {

    @Autowired
    private TypeService typeService;


    @GetMapping("/types")
    public String types(@PageableDefault(size = 10,sort = {"id"},direction = Sort.Direction.DESC) Pageable pageable, Model model){

        model.addAttribute("page",typeService.listType(pageable));
        System.out.println(typeService.listType(pageable));
        return "admin/types";
    }

    @GetMapping("/types/input")
    public String input(Model model){
        model.addAttribute("type",new Type());
        return "admin/types-input";
    }

    @GetMapping("/types/{id}/input")
    public String editInput(@PathVariable Long id, Model model){
        model.addAttribute("type",typeService.getType(id));
        return "admin/types-input";
    }

    @PostMapping("/types")
    /**
     * @Valid 对type进行校验
     * BindingResult result 校验结果
     * 这两个一定要紧挨着
     */
    public String post(@Valid Type type, BindingResult result, RedirectAttributes redirectAttributes){
        if(result.hasErrors()){
            return "admin/types-input";
        }
        List<Type> type1 = typeService.getTypeByName(type.getName());
        if (type1 !=null && !type1.isEmpty()){
            result.rejectValue("name","nameExist","该分类已存在！！");
            return "admin/types-input";
        }
        Type type2 = typeService.saveType(type);
        if (type2 == null ){
//            redirectAttributes.addFlashAttribute("message","分类新增失败！！！");
            result.rejectValue("name","addError","分类新增失败！！！");
            return "admin/types-input";
        }else{
            redirectAttributes.addFlashAttribute("message","新增分类成功！");
            return "redirect:/admin/types";
        }

    }

    @PostMapping("/types/{id}")
    public String editPost(@Valid Type type, BindingResult result,@PathVariable Long id, RedirectAttributes redirectAttributes){
        if(result.hasErrors()){
            return "admin/types-input";
        }
        List<Type> type1 = typeService.getTypeByName(type.getName());
        if (type1 !=null && !type1.isEmpty()){
            result.rejectValue("name","nameExist","该分类已存在！！");
            return "admin/types-input";
        }
        Type type2 = typeService.updateType(id,type);
        if (type2 == null ){
//          redirectAttributes.addFlashAttribute("message","分类新增失败！！！");
            result.rejectValue("name","addError","分类修改失败！！！");
            return "admin/types-input";
        }else{
            redirectAttributes.addFlashAttribute("message","分类修改成功！");
            return "redirect:/admin/types";
        }

    }

    @GetMapping("/types/{id}/delete")
    public String delete(@PathVariable Long id,RedirectAttributes attributes){

        if (!typeService.idIsExist(id)){
            attributes.addFlashAttribute("message","分类已不存在！！");
            return "redirect:/admin/types";
        }
        typeService.deleteType(id);
        attributes.addFlashAttribute("message","分类删除成功！");
        return "redirect:/admin/types";
    }
}
