package com.supfit.blog.controller.admin;

import com.supfit.blog.po.Blog;
import com.supfit.blog.po.User;
import com.supfit.blog.service.BlogService;
import com.supfit.blog.service.TagService;
import com.supfit.blog.service.TypeService;
import com.supfit.blog.vo.BlogQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;

/**
 * @author supfit
 *
 */

@Controller
@RequestMapping("/admin")
public class BlogController {

    private static final String INPUT = "admin/blogs-input";
    private static final String LIST = "admin/blogs";
    private static final String REDIRECT_LIST = "redirect:/admin/blogs";

    @Autowired
    private BlogService blogService;
    @Autowired
    private TypeService typeService;
    @Autowired
    private TagService tagService;

    @GetMapping("/blogs")
    /**
     * 博客列表初始化
     */
    public String blogs(@PageableDefault(size = 7,sort ={"updateTime"},direction = Sort.Direction.DESC ) Pageable pageable, BlogQuery blog, Model model){

        model.addAttribute("types",typeService.listType());
        model.addAttribute("page",blogService.listBlog(pageable,blog));
        return LIST;
    }

    @GetMapping("/blogs/input")
    /**
     * 新增博客初始化
     */
    public String input(Model model){
        model.addAttribute("types",typeService.listType());
        model.addAttribute("tags",tagService.listTag());
        model.addAttribute("blog",new Blog());
        return INPUT;
    }

    @GetMapping("/blogs/{id}/input")
    /**
     * 修改博客初始化
     */
    public String editInput(@PathVariable Long id, Model model){
        model.addAttribute("types",typeService.listType());
        model.addAttribute("tags",tagService.listTag());
        Blog blog = blogService.getBlog(id);
        blog.init();
        model.addAttribute("blog",blog);
        return INPUT;
    }


    @PostMapping("/blogs/search")
    /**
     * 搜索博客
     */
    public String searchBlogs(@PageableDefault(size = 7,sort ={"updateTime"},direction = Sort.Direction.DESC ) Pageable pageable, BlogQuery blog, Model model){

        model.addAttribute("page",blogService.listBlog(pageable,blog));
        return "admin/blogs :: blogList";
    }

    @PostMapping("/blogs")
    /**
     * 新增博客
     */
    public String post(Blog blog, HttpSession session, RedirectAttributes redirectAttributes){

        blog.setUser((User) session.getAttribute("user"));
        blog.setType(typeService.getType(blog.getType().getId()));
        blog.setTags(tagService.listTag(blog.getTagIds()));
        Blog b = blogService.saveBlog(blog);
        if (b == null){
            redirectAttributes.addFlashAttribute("message","添加博客失败！！！");
            return REDIRECT_LIST;
        }else{
            redirectAttributes.addFlashAttribute("message","添加博客成功！");
            return REDIRECT_LIST;
        }

    }

    @PostMapping("/blogs/{id}")
    /**
     * 修改博客
     */
    public String editPost(Blog blog, @PathVariable Long id,HttpSession session, RedirectAttributes redirectAttributes){

        blog.setUser((User) session.getAttribute("user"));
        blog.setType(typeService.getType(blog.getType().getId()));
        blog.setTags(tagService.listTag(blog.getTagIds()));
        Blog b = blogService.updateBlog(id,blog);
        if (b == null){
            redirectAttributes.addFlashAttribute("message","编辑博客失败！！！");
            return REDIRECT_LIST;
        }else{
            redirectAttributes.addFlashAttribute("message","编辑博客成功！");
            return REDIRECT_LIST;
        }
    }

    @GetMapping("/blogs/{id}/delete")
    /**
     * 删除博客
     */
    public String delete(@PathVariable Long id ,RedirectAttributes redirectAttributes){

        if (!blogService.idIsExist(id)) {
            redirectAttributes.addFlashAttribute("message", "博客已不存在！");
            return REDIRECT_LIST;
        }
        blogService.deleteBlog(id);
        redirectAttributes.addFlashAttribute("message","博客删除成功！");
        return REDIRECT_LIST;
    }
}
