package com.supfit.blog.service;

import com.supfit.blog.dao.TypeRepository;
import com.supfit.blog.exception.NotFoundException;
import com.supfit.blog.po.Type;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author supfit
 */

@Service
@Transactional //事务
public class TypeServiceImpl implements TypeService {

    @Autowired
    private TypeRepository typeRepository;

    @Override
    public Type saveType(Type type) {
        return typeRepository.save(type);
    }

    @Override
    public Type getType(Long id) {
        return typeRepository.getOne(id);
    }

    @Override
    public List<Type> getTypeByName(String name) {
        return typeRepository.findByName(name);
    }

    @Override
    public List<Type> listType() {
        Sort orders = Sort.by("id").descending();
        return typeRepository.findAll(orders);
    }

    @Override
    public List<Type> listTypeTop(Integer size) {

        Pageable pageable = PageRequest.of(0,size, Sort.Direction.DESC,"blogs.size");
        return typeRepository.findTop(pageable);
    }

    @Override
    public Page<Type> listType(Pageable pageable) {
        return typeRepository.findAll(pageable);
    }

    @Override
    public Type updateType(Long id, Type type) {
        Type t = typeRepository.getOne(id);
        if (t == null){
            throw new NotFoundException("该分类不存在");
        }
        BeanUtils.copyProperties(type,t);
        return typeRepository.save(t);
    }

    @Override
    public void deleteType(Long id) {
        typeRepository.deleteById(id);
    }

    @Override
    public Boolean idIsExist(Long id) {
        return typeRepository.existsById(id);
    }
}
