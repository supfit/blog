package com.supfit.blog.service;

import com.supfit.blog.po.User;

/**
 * @author supfit
 */
public interface UserService {

    User checkUser(String username,String password);
}
