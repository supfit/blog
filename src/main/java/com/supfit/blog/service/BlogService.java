package com.supfit.blog.service;

import com.supfit.blog.po.Blog;
import com.supfit.blog.vo.BlogQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @author supfit
 */

public interface BlogService {

    /**
     * 根据id搜索博客
     * @param id
     * @return
     */
    Blog getBlog(Long id);

    /**
     *获取博客内容并转换成HTML
     * @param id
     * @return
     */
    Blog getAndConvert(Long id);

    /**
     * 分页搜索和组合条件查询
     * @param pageable
     * @param blog
     * @return
     */
    Page<Blog> listBlog(Pageable pageable, BlogQuery blog);

    /**
     * 分页搜索
     * @param pageable
     * @return
     */
    Page<Blog> listBlog(Pageable pageable);

    /**
     * 根据tagId查询blog
     * @param tagId
     * @param pageable
     * @return
     */
    Page<Blog> listBlog(Long tagId,Pageable pageable);

    /**
     * 根据query自定义分页查询
     * @param query
     * @param pageable
     * @return
     */
    Page<Blog> listBlog(String query,Pageable pageable);

    /**
     * 获取最新的size条博客
     * @param size
     * @return
     */
    List<Blog> listBlogTop(Integer size);

    /**
     * 博客归档
     * @return
     */
    Map<String, List<Blog>> archiveBlog();

    /**
     * 博客条数
     * @return
     */
    Long blogCount();


    /**
     * 新增博客
     * @param blog
     * @return
     */
    Blog saveBlog(Blog blog);

    /**
     * 更新博客
     * @param id
     * @param blog
     * @return
     */
    Blog updateBlog(Long id, Blog blog);

    /**
     * 删除博客
     * @param id
     */
    void deleteBlog(Long id);

    /**
     * 查询博客id是否存在
     * @param id
     * @return
     */
    boolean idIsExist(Long id);

}
