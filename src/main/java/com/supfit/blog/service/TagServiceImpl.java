package com.supfit.blog.service;

import com.supfit.blog.dao.TagRepository;

import com.supfit.blog.exception.NotFoundException;
import com.supfit.blog.po.Tag;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @author supfit
 */

@Service
@Transactional //事务
public class TagServiceImpl implements TagService {

    @Autowired
    private TagRepository tagRepository;

    @Override
    public Tag saveTag(Tag tag) {
        return tagRepository.save(tag);
    }

    @Override
    public Tag getTag(Long id) {
        return tagRepository.getOne(id);
    }

    @Override
    public List<Tag> getTagByName(String name) {
        return tagRepository.findByName(name);
    }

    @Override
    public List<Tag> listTag() {
        Sort orders = Sort.by("id").descending();
        return tagRepository.findAll(orders);
    }

    @Override
    public List<Tag> listTagTop(Integer size) {
        Pageable pageable =PageRequest.of(0,size, Sort.Direction.DESC,"blogs.size");
        return tagRepository.findTop(pageable);
    }

    @Override
    public List<Tag> listTag(String ids) {

        return tagRepository.findAllById(convertToList(ids));
    }

    /**
     * 字符串转换为数组
     * @param str
     * @return
     */
    private List<Long> convertToList(String str){
        List<Long> list = new ArrayList<>();
        if (!"".equals(str) && str!=null){
            String[] array = str.split(",");
            for (int i = 0; i < array.length; i++) {
                list.add(new Long(array[i]));
            }
        }
        return list;
    }

    @Override
    public Page<Tag> listTag(Pageable pageable) {
        return tagRepository.findAll(pageable);
    }

    @Override
    public Tag updateTag(Long id, Tag tag) {
        Tag t = tagRepository.getOne(id);
        if (t == null){
            throw new NotFoundException("该分类不存在");
        }
        BeanUtils.copyProperties(tag,t);
        return tagRepository.save(t);
    }

    @Override
    public void deleteTag(Long id) {
        tagRepository.deleteById(id);
    }

    @Override
    public Boolean idIsExist(Long id) {
        return tagRepository.existsById(id);
    }
}
