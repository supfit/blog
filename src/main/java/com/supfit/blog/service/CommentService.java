package com.supfit.blog.service;

import com.supfit.blog.po.Comment;

import java.util.List;

/**
 * @author supfit
 */
public interface CommentService {

    /**
     * 根据博客id获取评论列表
     * @param blogId
     * @return
     */
    List<Comment> listCommentByBlogId(Long blogId);

    /**
     * 保存评论信息
     * @param comment
     * @return
     */
    Comment saveComment(Comment comment);
}
